[English](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_en.md) | [Korean](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_ko.md) | [French](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_fr.md) | [Russian](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_ru.md)

# <b1>Ukrainian:</b1>

# UA Cyber Shield

<br />🔥Новий кроссплатформенний софт вже готовий до бою - https://github.com/opengs/uashield 🔥
<br />▪ Доступно на macOS, Linux, Windows
<br />▪ Легко встановити на всі доступні платформи, ручаюсь за безпеку!
<br />▪ Цілі координуюся адмінами основних чатів з DDoS
<br />▪ Бажаючі допомогти з проксі, все ще можуть писати в telegram: @esen1n25
<br />▪ Не забувайте ставити зірочки авторам за працю )

# NoWarDDoS

DDoS Russian websites to help Ukraine to win this hybrid war
<br />
<br />▪ Встановлюємо Python 3.8+ (Обов'язково ставимо галку "Add to path")
![alt text](https://miro.medium.com/max/1344/0*7nOyowsPsGI19pZT.png)
<br />▪ Відкриваємо термінал(консоль), переходимо в корінь, куди розпакували нашу програму командою cd
<br />▪ В корені вводимо наступну команду:
<br /> Windows: python main.py
<br /> macOS/Linux: python3 main.py 
<br />
<br />▪ На 8 CPU і 16 гігів оперативки ставим 500 потоків. Проксі встановлюється автоматично.
<br />▪ Експерементуйте, обирайте оптимальну кількість потоків, щоб проц в сотку довбився ))
<br />▪ Приклад: python3 main.py 500
<br />

#

<br />▪ Все працює через проксі, не бійтесь!
<br />▪ Якщо виникнуть проблеми, 24/7 відповідаємо та координуємо через чат Telegram: https://t.me/+wnvf4Dv8AQwxMjVi
<br />▪ Програма оновлюється автоматично, вона сама оновиться та знову запустить атаку, оновленния перевіряються кожну хвилину
<br />▪ Якщо помітили в чаті оновлення яке я не виклав, повідомте в тг
<br />
<br />▪ Якщо вибиває помилку пов'язану з ModuleNotFoundError aбо інші, спробуйте:
<br /> Windows: python -m pip install --upgrade pip
<br /> pip install -r requirements.txt
<br /> macOS/Linux: python3 -m pip install --upgrade pip
<br /> pip3 install -r requirements.txt
<br /> macOS ARM (M1) все працює!
<br />
<br />▪ Якщо не допомагає, обов'язково пишіть в тг!
<br />**Слава Україні!**
## Інструкція для запуску у `Docker`:

1. Ставимо [докер](https://www.docker.com/)
2. Пулаєм імадж (при обновах репи запускати те саме щоб стягнути апдейт)

```shell
docker pull registry.gitlab.com/a_gonda/nowarddos:latest
```

3. Запускаємо

```shell
docker run --rm registry.gitlab.com/a_gonda/nowarddos:latest 500
```

## Інструкця для запуску у `Kubernetes`:

https://github.com/saladar/bellaciao