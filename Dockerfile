FROM python:alpine
LABEL org.nowar.version="7"

COPY *.py /nowarddos/
COPY requirements.txt /nowarddos/

WORKDIR /nowarddos
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "/nowarddos/main.py"]
