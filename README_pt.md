[English](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_en.md) | [Korean](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_ko.md) | [French](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_fr.md) | [Russian](https://github.com/AlexTrushkovsky/NoWarDDoS/blob/main/README_ru.md) | [Portuguese](https://github.com/phmpg/NoWarDDoS/blob/main/README_pt.md) |
# <b1>Português:</b1>

# UA Cyber Shield

<br />🔥O novo software multiplataforma pronto para a batalha - https://github.com/opengs/uashield 🔥
<br />▪ Disponível em macOS, Linux, Windows
<br />▪ Fácil de instalar em todas as plataformas disponíveis, garanto segurança!
<br />▪ As metas são coordenadas pelos administradores dos principais chats com DDoS
<br />▪ Quem quiser ajudar com o proxy ainda pode escrever no telegrama: @esen1n25
<br />▪ Não se esqueça de colocar estrelas para o trabalho dos autores )

# NoWarDDoS

Sites russos, DDoS para ajudar a Ucrânia a vencer esta guerra híbrida
<br />
<br />▪ Instale o Python 3.8+ (Certifique-se de marcar a caixa "Add to path")
![alt text](https://miro.medium.com/max/1344/0*7nOyowsPsGI19pZT.png)
<br />▪ Abra o terminal (console), vá até a raiz, onde descompactamos nosso programa com o comando cd
<br />▪ Na raiz, digite o seguinte comando:
<br /> Windows: python attack.py NÚMERO * DE FLUXOS
<br /> macOS/Linux: python3 attack.py NÚMERO * DE FLUXOS
<br />
<br />▪ Em 8 CPU e 16 GB de RAM colocamos 500 threads. O proxy é instalado automaticamente.
<br />▪ Experimente, limpe o número ideal de fluxos, para que a porcentagem por cem metros quadrados ))
<br />▪ Você pode adicionar a flag -v para ver os códigos de resposta
<br />▪ Você pode adicionar a flag -n para que os logs não sejam limpos
<br />▪ Você pode adicionar a flag -p para ver o proxy
<br />▪ Exemplo: python3 attack.py 500 -v -n
<br />

#

<br />▪ Tudo funciona através de um proxy, não tenha medo!
<br />▪ Se houver problemas, respondemos e coordenamos 24 horas por dia, 7 dias por semana, via chat do Telegram: https://t.me/+wnvf4Dv8AQwxMjVi
<br />▪ O programa é atualizado automaticamente, ele se atualizará e executará o ataque novamente, as atualizações são verificadas a cada minuto
<br />▪ Atualização do aplicativo automaticamente e puxará toda a última lista de sites
<br />
<br />▪ Se você tiver erros como ModuleNotFoundError etc. tente:
<br /> Windows: python -m pip install --upgrade pip
<br /> pip install -r requirements.txt
<br /> macOS/Linux: python3 -m pip install --upgrade pip
<br /> pip3 install -r requirements.txt
<br />
<br /> ~~O macOS com ARM (M1) não é suportado temporariamente, espere atualizações nas próximas horas~~
<br /> Suporte para macOS ARM (M1)!
<br />
<br />▪ Se não ajudar, não deixe de escrever em tg!!
<br />**Glória à Ucrânia!**

## Como criar uma imagem do Docker:

1. Download [docker](https://www.docker.com/)
2. Puxe a imagem:
```shell
docker pull gcr.io/fuck-russia-342819/nowarddos:latest
```
3.	Rode:
```shell
docker run --rm gcr.io/fuck-russia-342819/nowarddos:latest 500
```

## Como construir o `Kubernetes`:

https://github.com/saladar/bellaciao