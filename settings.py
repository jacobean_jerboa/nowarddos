from functools import lru_cache
from typing import List

from pydantic import BaseSettings

_DEFAULT_SITES_HOSTS = [
    "https://gitlab.com/jacobean_jerboa/nowarddos/-/snippets/2265934/raw/main/snippetfile1.txt"
]
_DEFAULT_PROXIES_HOSTS = ["https://raw.githubusercontent.com/xfreed/Proxy-List/main/Proxy_Http.txt"]


class Settings(BaseSettings):
    VERSION: int = 9
    MAX_THREADS: int = 500
    SITES_HOSTS: List[str] = _DEFAULT_SITES_HOSTS
    PROXIES_HOSTS: List[str] = _DEFAULT_PROXIES_HOSTS
    MAX_REQUESTS_TO_SITE: int = 200
    TARGET_UPDATE_RATE: int = 30
    READ_TIMEOUT: int = 10
    BROWSER: dict = {'browser': 'firefox', 'platform': 'android', 'mobile': True}
    HEADERS_TEMPLATE: dict = {
        'Content-Type': 'application/json',
        'cf-visitor': 'https',
        'User-Agent': None,
        'Connection': 'keep-alive',
        'Accept': 'application/json, text/plain, */*',
        'Accept-Language': 'ru',
        'x-forwarded-proto': 'https',
        'Accept-Encoding': 'gzip, deflate, br'
    }


@lru_cache()
def get_settings():
    return Settings()
